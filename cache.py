import os
import robot as r


class Cache:
    def __init__(self):
        self.all_urls = []

    def retrieve(self, url):
        aux_robot = r.Robot(url)
        if url not in self.all_urls:
            self.all_urls.append(url)
        if aux_robot.filename not in os.listdir():
            aux_robot.retrieve()

    def show(self, url):
        aux_robot = r.Robot(url)
        bool_downloaded = aux_robot.show()
        if bool_downloaded:
            self.all_urls.append(url)

    def show_all(self):
        print(self.all_urls)

    def content(self, url):
        aux_robot = r.Robot(url)
        if aux_robot.filename not in os.listdir():
            self.retrieve(url)
        returned_content = aux_robot.content()
        return returned_content
