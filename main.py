import robot as r
import cache as c

if __name__ == "__main__":
    url = "https://www.google.com/"
    bot = r.Robot(url)
    bot.retrieve()
    bot.show()
    bot.content()
    url = "https://www.python.org/"
    bot = r.Robot(url)
    bot.show()
    bot.retrieve()  # Will do nothing as the url has already been downloaded
    bot.content()

    url_cache = c.Cache()
    url_cache.retrieve("https://www.amazon.es/")
    url_cache.show("https://www.amazon.es/")
    content_str1 = url_cache.content("https://www.amazon.es/")  # Use debugger to see the value
    url_cache.show_all()
    url_cache.show("https://www.apple.com/")
    content_str2 = url_cache.content("https://www.nvidia.com/")  # Use debugger to see the value
    url_cache.show_all()

